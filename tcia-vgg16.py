import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import keras
import pydicom as dicom
import os
import png
from keras.utils import plot_model
import pandas as pd
from keras.preprocessing.image import ImageDataGenerator
from keras.applications import ResNet50
from keras.applications import VGG16
from keras.applications.resnet50 import preprocess_input
from keras import Model, layers
from keras.models import load_model, model_from_json	
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import array_to_img
from keras.layers import BatchNormalization
import keras.backend as K

conv_base = VGG16(include_top=False,
                     weights='imagenet',
                     input_shape = (224,224,3))
                     
                     
for layer in conv_base.layers:
    layer.trainable = False
    x = conv_base.output 
    x = layers.GlobalAveragePooling2D()(x)
    x = layers.Dense(128, activation='relu')(x)
    #x = layers.BatchNormalization()(x)
    x = layers.Dropout(rate = 0.2)(x)
    predictions = layers.Dense(2, activation='softmax')(x)
    
optimizer = keras.optimizers.Adam(lr=0.001, decay = 0.0002)

model.compile(loss='sparse_categorical_crossentropy',
              optimizer=optimizer,
              metrics=['accuracy'])

history = model.fit_generator(
    generator=train_generator,
    epochs=50,
    steps_per_epoch= 547,
    validation_steps = 157,
    validation_data=validation_generator)
    model = Model(conv_base.input, predictions)